use bikestores;

-- UNION
SELECT first_name, last_name,phone , 'cu' as identifiant FROM customers
UNION 
SELECT first_name, last_name, phone ,'st'  FROM staffs;

-- UNION ALL
SELECT first_name, last_name,phone , 'cu' as identifiant FROM customers
UNION ALL
SELECT first_name, last_name, phone ,'st'  FROM staffs;

-- INTERSECT
SELECT city FROM customers
INTERSECT
SELECT city FROM stores;

-- EXCEPT
SELECT product_id FROM products
EXCEPT
SELECT product_id FROM order_items;
