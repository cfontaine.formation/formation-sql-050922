USE world;
-- Afficher les 10 pays les plus peupl�s (nom et population)
SELECT TOP(10) name, population FROM country ORDER BY population DESC;

-- Nombre de pays pr�sents dans la table Country
SELECT COUNT(code) nombre_pays FROM country;

-- Afficher les continents et leur la population totale class�
-- du plus peupl� au moins peupl�
SELECT continent, sum(CAST(population AS BIGINT))  total_population FROM country GROUP BY continent ORDER BY total_population DESC;  

-- Nombre de pays par continent
SELECT count(code) pays_par_continent FROM country GROUP BY continent;

USE bikestores;
SELECT COUNT(DISTINCT city) FROM customers WHERE city LIKE 'L__%';  