USE world;
-- Afficher chaque pays et sa capitale
SELECT country.name as pays, city.name as capital FROM country
INNER JOIN city
ON country.capital= city.id;

-- Afficher pays et ville classer par pays puis par ville en ordre alphabetique 
-- (ne pas afficher les pays sans ville)
SELECT country.name pays,city.name ville
FROM city INNER JOIN country
ON city.country_code= country_code
ORDER BY country.name , city.name;

-- Pays sans ville
SELECT country.name
FROM country
LEFT JOIN city
ON country.code = city.country_code
WHERE city.country_code IS NULL;

-- nom pays, langue, pourcentage , class� par pays, pourcentage d�croissant
SELECT country.name, country_language.languages,percentage
FROM country
RIGHT JOIN country_language
ON country_language.country_code=country.code
ORDER BY country.name, percentage DESC

-- Nombre de langue officielle par pays - class� par nbr de langue officielle
SELECT country.name pays ,COUNT(country_code) nombre_langue
FROM country_language
LEFT JOIN country
ON country_language.country_code=country.code
WHERE country_language.is_official='T'
GROUP BY country.name
ORDER BY nombre_langue,pays;
