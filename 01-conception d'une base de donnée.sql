/* 01 - Conception de base */

/* Commentaire sur
plusieurs lignes */
-- Créer un schema 
-- Commentaire sur ligne ou fin de ligne
USE master;
-- Suprimer la table exemple si elle existe
DROP DATABASE IF EXISTS exemple;

-- Créer une base de données example
CREATE DATABASE example;

-- Choisir exemple comme base de données courante 
USE example;

-- Afficher toutes les bases de données du serveur
EXEC sp_databases;

-- Modifier le nom de la base
ALTER DATABASE example  
MODIFY NAME=exemple

-- Création d'une table 
CREATE TABLE christophe_films(
	titre_vf VARCHAR(100) NOT NULL, -- NOT NULL -> titre_vf ne peut plus être égal à NULL, lorsque l'on ajoutera des données, on sera obligé de données un valeur à titre_vf
	titre_vo VARCHAR(100),
	synopsis VARCHAR(2000),
	date_sortie DATE , 
	note TINYINT,
	duree SMALLINT DEFAULT 120 -- DEFAULT -> permet de définir une valeur par défaut autre que NULL
);

-- Lister les tables de la base
SELECT * FROM INFORMATION_SCHEMA.TABLES
WHERE TABLE_TYPE='BASE TABLE'

-- Afficher la description de la table christophe_films
EXEC sp_columns christophe_films;

-- MODIFICATION DES TABLES
-- Renommer une table
EXEC sp_rename 'christophe_films','films';

-- Ajouter une colonne affiche de type IMAGE à la table
ALTER TABLE films ADD affiche IMAGE;

-- Supprimer une colonne affiche dans la table films
ALTER TABLE films DROP COLUMN affiche ;

-- Modifier le type de la colonne VARCHAR(2000) => VARCHAR(2500)
ALTER TABLE films ALTER COLUMN synopsis VARCHAR(2500);

-- Renommer une colonne
EXEC sp_rename 'films.date_sortie', 'date_premiere','COLUMN';

-- Ajouter une contrainte NOT NULL à la colonne date_premiere
ALTER TABLE films ALTER COLUMN date_premiere DATE NOT NULL;

-- Ajouter une colonne avec une contrainte UNIQUE
ALTER TABLE films ADD num_identifiant CHAR(25) NOT NULL UNIQUE;

-- Ajouter la contrainte DEFAULT à la colonne note
ALTER TABLE films ADD CONSTRAINT df_note DEFAULT 5 FOR note;

-- Ajout d'une clé primaire (la colonne num_identifiant) à la table films
ALTER TABLE films ADD PRIMARY KEY (num_identifiant);

-- Créer une table nations avec une clé primaire id
CREATE TABLE nations(
	id INT IDENTITY PRIMARY KEY,
	nom VARCHAR(60)
);

-- Clé étrangère

-- Relation 1-n
-- un film n'a qu'une seule nationalité et une nation peut être associée à plusieurs films

-- Ajout d'une clé etrangère à une table existante (films)
-- Ajouter une colonne à la table films
ALTER TABLE films ADD id_nations INT;

-- Ajouter une contrainte clé étrangère
ALTER TABLE films ADD
CONSTRAINT fk_films_nations	--nom de la contrainte
FOREIGN KEY (id_nations)
REFERENCES nations (id);

-- Relation n-n
-- Un film peut avoir plusieurs genres et un peut être associée à plusieurs films
CREATE TABLE genres(
	id INT IDENTITY PRIMARY KEY,
	nom VARCHAR(40)
);

--  Relation n-n => Table de jointure entre les films et les genres
CREATE TABLE films_genres(
	id_genres INT,
	num_identifiant CHAR(25),
	-- contrainte clé étrangère => films
	CONSTRAINT fk_films_genres
	FOREIGN KEY (num_identifiant)
	REFERENCES films(num_identifiant),
	
	--  contrainte clé étrangère => genres
	CONSTRAINT fk_genres_films
	FOREIGN KEY (id_genres)
	REFERENCES genres(id),
	
	-- clé primaire composé des 2 colonnes id_genres, num_identifiant
	CONSTRAINT pk_films_genres
	PRIMARY KEY (id_genres,num_identifiant)
);



