use world;

-- Les 10 langues officielles qui ont la meilleure Esp�rance de vie (doublon accept�)
SELECT TOP 10 life_expectancy esperance_de_vie, languages FROM country
INNER JOIN country_language
ON code=country_code
WHERE is_official='T'
ORDER BY life_expectancy DESC;

-- Liste pays et ville ayant moins de 1000 habitant
SELECT name, population FROM city WHERE population <1000
UNION
SELECT name, population FROM country WHERE population <1000;

-- Nombre de ville par pays  (et ordonner par nb ville)
SELECT count(id) nombre_ville, country.name
FROM city 
INNER JOIN country 
ON country_code=code
GROUP BY country.name
ORDER BY nombre_ville DESC;

-- Nombre de ville par pays  (et ordonner par nb ville)
SELECT city.name  FROM city
INNER JOIN country ON country.capital = city.id
INNER JOIN  country_language ON country_language.country_code = code
WHERE country_language.is_official = 'T' 
GROUP BY city.name
HAVING count(country_language.languages) = 1
ORDER BY count(country_language.languages);

-- Liste des pays o� on parle francais ou anglais
SELECT name  FROM country
INNER JOIN country_language
ON country_code = code
WHERE languages='english'
UNION
SELECT name  FROM country
INNER JOIN country_language
ON country_code = code
WHERE languages='french';

