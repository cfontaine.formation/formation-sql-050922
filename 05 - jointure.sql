SELECT livres.id,titre,annee,genres.nom,auteurs.prenom,auteurs.nom,pays.nom
FROM genres
INNER JOIN livres
ON livres.genre=genres.id
INNER JOIN livre2auteur
ON livres.id=livre2auteur.id_livre
INNER JOIN auteurs
ON auteurs.id=livre2auteur.id_auteur
INNER JOIN pays
ON auteurs.nation=pays.id;

SELECT * FROM livres CROSS JOIN genres;

SELECT genres.nom, genres.id
FROM genres
LEFT JOIN livres 
ON livres.genre=genres.id
WHERE titre IS NULL;

SELECT genres.nom, genres.id
FROM livres
RIGHT JOIN genres 
ON livres.genre=genres.id
WHERE titre IS NULL;

SELECT genres.nom, genres.id,titre
FROM livres
FULL JOIN genres 
ON livres.genre=genres.id

USE  bikestores;
SELECT employe.staff_id,CONCAT(employe.first_name,' ',employe.last_name) As employe_name,
 manager.staff_id, CONCAT(manager.first_name,' ',manager.last_name) AS manager_name 
FROM staffs AS employe
LEFT JOIN staffs AS manager
ON employe.manager_id=manager.staff_id;

