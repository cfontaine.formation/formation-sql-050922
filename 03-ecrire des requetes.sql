USE bibliotheque;

SELECT titre, annee FROM livres;
SELECT id,titre,annee,genre FROM livres;
SELECT * FROM livres;

SELECT DISTINCT prenom FROM auteurs; 

SELECT prenom FROM auteurs; 
-- Alias
SELECT titre, 'age', 2022-annee FROM livres;

SELECT titre AS titrelivre,  2022-annee  age,annee  FROM livres; 

SELECT titre, annee FROM livres book;

SELECT titre FROM livres WHERE annee=1964;

SELECT titre FROM livres WHERE annee>1980;

SELECT titre, annee FROM livres WHERE annee=1955 OR annee<1900;

SELECT titre, annee FROM livres WHERE annee>=1950 AND annee<1960;

SELECT titre, annee FROM livres WHERE NOT(annee=1955 OR annee<1900);

-- SELECT prenom, nom FROM auteurs WHERE deces<>NULL;

SELECT prenom, nom FROM auteurs WHERE deces IS NULL;

SELECT titre, annee FROM livres WHERE annee=1955 OR annee=1960 OR annee=1973;

SELECT titre, annee FROM livres WHERE annee IN (1955,1960,1973);

SELECT titre, annee FROM livres WHERE annee BETWEEN 1950 AND 1960;

SELECT prenom ,nom FROM auteurs WHERE naissance BETWEEN '1900-01-01' AND '1933-10-23';

SELECT prenom ,nom FROM auteurs WHERE nom BETWEEN 'ed' AND 'kevin';

SELECT titre FROM livres WHERE titre LIKE 'd__%';

SELECT titre FROM livres WHERE titre LIKE 'l%r';

SELECT titre FROM livres WHERE titre LIKE '[a-d]__%';

SELECT titre FROM livres WHERE titre LIKE '[^a-d]__%';

SELECT annee,titre FROM livres WHERE annee>1970 ORDER BY annee,titre DESC;

SELECT annee,titre FROM livres WHERE annee>1950 ORDER BY genre;


SELECT TOP(10)annee,titre FROM livres ORDER BY annee DESC;

SELECT TOP(30) PERCENT annee,titre FROM livres ORDER BY annee DESC;

SELECT TOP(7) WITH TIES titre, annee FROM livres ORDER BY annee DESC;


SELECT annee,titre FROM livres ORDER BY annee DESC OFFSET 1 ROW FETCH NEXT 5 ROW ONLY;

SELECT COUNT(*) FROM livres;


SELECT annee,COUNT(*) nb_livre FROM livres GROUP BY annee;

SELECT genre,COUNT(*) nb_livre FROM livres WHERE annee>1970 GROUP BY genre ;

SELECT genre,COUNT(*) nb_livre 
FROM livres 
WHERE annee>1970 
GROUP BY genre 
HAVING COUNT(*)>5;
USE WORLD;
