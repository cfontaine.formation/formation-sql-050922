USE exemple;

CREATE TABLE exemple_nations(
id INT IDENTITY PRIMARY KEY,
nom VARCHAR(60)
);

CREATE TABLE exemple_films(
id INT IDENTITY PRIMARY KEY,
titre_vf VARCHAR(100) NOT NULL,
titre_vo VARCHAR(100),
synopsis VARCHAR(2000),
date_sortie DATE NOT NULL,
note TINYINT DEFAULT 5,
duree SMALLINT,
id_nations INT,
CONSTRAINT fk_films_nations_exemple
FOREIGN KEY (id_nations)
REFERENCES exemple_nations(id)
);
INSERT INTO exemple_nations VALUES('france');
INSERT INTO exemple_nations VALUES('�tats-unis');

INSERT INTO exemple_films(titre_vf,date_sortie,duree,id_nations)
VALUES ('Reservoir dog','1993-06-01',125,2);

INSERT INTO exemple_films(date_sortie,duree,id_nations,titre_vf)
VALUES ('1993-06-01',125,1,'Les visiteurs');

INSERT INTO exemple_films(date_sortie,duree,id_nations,titre_vf)
VALUES ('1993-06-01',125,1,'Les visiteurs 2');

INSERT INTO exemple_films(date_sortie,duree,id_nations,titre_vf)
VALUES
('1986-07-01',120,2,'Retour vers le futur'),
('1987-07-01',120,2,'Retour vers le futur 2'),
('1988-07-01',120,2,'Retour vers le futur 3');


SELECT * FROM exemple_films;

DELETE FROM exemple_nations WHERE id=3;

INSERT INTO exemple_nations VALUES('Irlande');

DELETE FROM exemple_nations WHERE nom='Irlande';

TRUNCATE TABLE exemple_films;
INSERT INTO exemple_films(date_sortie,duree,id_nations,titre_vf)
VALUES
('1986-07-01',120,2,'Retour vers le futur'),
('1987-07-01',120,2,'Retour vers le futur 2'),
('1988-07-01',120,2,'Retour vers le futur 3');

DELETE FROM exemple_films;




UPDATE exemple_films
SET titre_vf='Les visiteurs' Where id=8;




UPDATE exemple_films
SET duree=duree-10;